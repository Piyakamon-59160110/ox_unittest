/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxunittest.Player;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.mycompany.oxunittest.Table;
/**
 *
 * @author Admin
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }     
    public void testCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testRow2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCol2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());
    }
    public void testRow3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCol3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testEsWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testSsWin() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testDraw() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testSwitchPlayer() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.switchPlayer();
        assertEquals('x', table.getCurrentPlayer().getName());
    }
    public void testSwitchPlayer2() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(x, o);
        table.switchPlayer();
        assertEquals('o', table.getCurrentPlayer().getName());
    }
}
